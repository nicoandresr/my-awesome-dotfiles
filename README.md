# Description
my dotfiles

# Usage
```
ln -s ~/.config/dotfiles/xbindkeysrc ~/.xbindkeysrc
```

# Extended keyboard
use `xbindkeys --key` to identify the key you wanna use
